# quotable-public

## Quotable

Quotable is a Flutter web application and Android app that allows users to see random quotes from a variety of authors.

## Description

Quotable is a Flutter web application and Android app that allows users to see random quotes from a variety of authors.

Quotable is a work in progress, and I am actively working on it.
Any data you store in the app may be lost with a future update.
All quotes are stored locally on the device, and no data is sent to any server.

## Starting

Quotable is available as a web app here: [https://pavelsafronov.gitlab.io/quotable/](https://pavelsafronov.gitlab.io/quotable/)

The Android app is available for download here: [https://pavelsafronov.gitlab.io/quotable/quotable.apk](https://pavelsafronov.gitlab.io/quotable/quotable.apk)

## Support

If you encounter any issues, please open an issue in this repository.
The source code for Quotable is private at the moment, but may be open-sourced in the future.

## Roadmap

- Add Firebase support
- Add user authentication
- Store user settings and favorite quotes in the cloud
- Add a way to share a list of quotes
- Add categories

## Contributing

If you would like to contribute to Quotable, please open an issue in this repository.

## Source

The source code for Quotable is private at the moment.
If you have access to it, you can find it here: [https://gitlab.com/PavelSafronov/quotable](https://gitlab.com/pavelsafronov/quotable)

## License

The source code for Quotable is private at the moment, but may be open-sourced in the future.
I am unsure how it will be licensed at the moment.

## Project status

I am actively working on this project.
I am planning on making the Android app available on the Google Play Store in the future.
